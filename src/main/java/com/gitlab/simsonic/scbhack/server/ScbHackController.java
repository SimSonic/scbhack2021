package com.gitlab.simsonic.scbhack.server;

import com.gitlab.simsonic.scbhack.server.api.GetUserResponse;
import com.gitlab.simsonic.scbhack.server.service.ScbHackUiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ScbHackController {

    private final ScbHackUiService scbHackUiService;

    /**
     * testuser:password123
     */
    public static final String LOGIN_PASSWORD = "dGVzdHVzZXI6cGFzc3dvcmQxMjM=";

    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<GetUserResponse> getUser(
            @RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String basicAuth,
            @PathVariable("userId") int userId
    ) {
        // ¯\_(ツ)_/¯
        String base64 = StringUtils.removeStartIgnoreCase(basicAuth, "Basic ");
        if (!LOGIN_PASSWORD.equals(base64)) {
            HttpHeaders headers = new HttpHeaders();
            headers.set("WWW-Authenticate", "Basic");
            return new ResponseEntity<>(headers, HttpStatus.UNAUTHORIZED);
        }

        log.debug("Request: {}", userId);

        GetUserResponse responseDto = scbHackUiService.getUser(userId);

        log.debug("Response: {}", responseDto);

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
