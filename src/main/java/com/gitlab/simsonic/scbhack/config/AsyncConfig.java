package com.gitlab.simsonic.scbhack.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAsync
@Configuration
@RequiredArgsConstructor
@Slf4j
public class AsyncConfig {

    private static final int TASK_EXECUTOR_CORE_POOL_SIZE = 2;
    private static final int TASK_EXECUTOR_MAXIMUM_POOL_SIZE = Integer.MAX_VALUE;
    private static final int TASK_EXECUTOR_QUEUE_CAPACITY = 2;
    private static final int TASK_EXECUTOR_THREAD_TTL = 2;

    /**
     * TaskExecutor, используемый для запуска асинхронных методов.
     */
    @Bean
    @Qualifier("taskExecutor")
    TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setThreadNamePrefix("async-");

        taskExecutor.setCorePoolSize(TASK_EXECUTOR_CORE_POOL_SIZE);
        taskExecutor.setMaxPoolSize(TASK_EXECUTOR_MAXIMUM_POOL_SIZE);
        taskExecutor.setQueueCapacity(TASK_EXECUTOR_QUEUE_CAPACITY);
        taskExecutor.setKeepAliveSeconds(TASK_EXECUTOR_THREAD_TTL);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);

        return taskExecutor;
    }
}
