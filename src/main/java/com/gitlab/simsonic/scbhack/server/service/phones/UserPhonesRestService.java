package com.gitlab.simsonic.scbhack.server.service.phones;

import com.gitlab.simsonic.scbhack.config.ScbHackProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserPhonesRestService {

    private final RestTemplate restTemplate;
    private final ScbHackProperties scbHackProperties;

    @Async
    public Future<UserPhonesResponseDto> getUserPhones(int userId) {

        String url = String.format("%s/api/v1/phones/%d",
                scbHackProperties.getRsEndpoint(),
                userId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<UserPhonesResponseDto> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                requestEntity,
                UserPhonesResponseDto.class);

        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            log.error("Error in REST service: {}", responseEntity);
            throw new IllegalStateException("Error in REST service.");
        }

        UserPhonesResponseDto result = Objects.requireNonNull(responseEntity.getBody());
        return AsyncResult.forValue(result);
    }
}
