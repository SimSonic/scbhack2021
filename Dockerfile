FROM bellsoft/liberica-openjdk-alpine-musl:11 AS builder

# Распаковываем .jar-файл.
WORKDIR            /extracted
COPY target/*.jar  /extracted/application.jar
RUN  jar -xf       /extracted/application.jar

FROM bellsoft/liberica-openjdk-alpine-musl:11

# Приложение будет работать во временной зоне Europe/Moscow.
ENV TZ=Europe/Moscow

# Установка обновлений пакетов и дополнительных инструментов.
# hadolint ignore=DL3018
RUN  apk --no-cache add jq \
                        mc \
                        curl wget \
                        htop \
                        tzdata \
  # Создаём пользователя для запуска контейнера не под root-ом.
  && mkdir -p                 /scbhack-user \
  && chmod 666                /scbhack-user \
  && adduser -S -D -u 3456 -h /scbhack-user  scbhack-user

# Приложение будет запускаться от имени созданного выше пользователя.
USER scbhack-user

# Скопировал из starter-kit.
ENV ws.endpoint=http://localhost:9080/ws
ENV rs.endpoint=http://localhost:9080
ENV server.port=9081

WORKDIR                                          /scbhack-user
COPY --from=builder /extracted/META-INF          /scbhack-user/META-INF
COPY --from=builder /extracted/BOOT-INF/lib      /scbhack-user/lib
COPY --from=builder /extracted/BOOT-INF/classes  /scbhack-user/classes

# Внешний порт приложения.
EXPOSE 8080
# Actuator.
EXPOSE 8081

ENTRYPOINT ["java", \
            # Разрешаем использовать Preview-фичи.
            # "-XX:+UnlockExperimentalVMOptions", \
            # "--enable-preview", \
            # Информативные NPE.
            # "-XX:+ShowCodeDetailsInExceptionMessages", \
            # Уменьшим стек в два раза.
            "-Xss512k", \
            # Запуск распакованного Fat Jar.
            "-classpath", ".:classes:lib/*", \
            "com.gitlab.simsonic.scbhack.ScbHackApplication" \
]
CMD []
