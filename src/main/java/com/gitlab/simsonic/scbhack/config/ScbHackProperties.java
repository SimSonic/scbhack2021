package com.gitlab.simsonic.scbhack.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("scbhack")
@Data
public class ScbHackProperties {

    private String wsEndpoint;
    private String rsEndpoint;
}
