package com.gitlab.simsonic.scbhack.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.lang.NonNull;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.function.Supplier;

@Configuration
@Slf4j
public class RestTemplatesConfig {

    private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(5);
    private static final Duration DEFAULT_READ_TIMEOUT = Duration.ofSeconds(5);

    /**
     * HttpComponentsClientHttpRequestFactory используется по той причине, что клиент, созданный стандартной
     * фабрикой SimpleClientHttpRequestFactory даже в случае использования NoOpResponseErrorHandler-а
     * при получении статуса 401 UNAUTHORIZED будет бросать исключение.
     */
    private static final Supplier<ClientHttpRequestFactory> HTTP_REQUEST_FACTORY = RestTemplatesConfig::createRequestFactory;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return createNoOpRestTemplate(restTemplateBuilder);
    }

    private static RestTemplate createNoOpRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .requestFactory(HTTP_REQUEST_FACTORY)
                .errorHandler(NoOpResponseErrorHandler.INSTANCE)
                .setConnectTimeout(DEFAULT_CONNECT_TIMEOUT)
                .setReadTimeout(DEFAULT_READ_TIMEOUT)
                .build();
    }

    private static ClientHttpRequestFactory createRequestFactory() {
        HttpClient httpClient = HttpClientBuilder.create()
                // Отключаем обработку куки.
                .disableCookieManagement()
                .build();

        return new HttpComponentsClientHttpRequestFactory(httpClient);
    }

    private static class NoOpResponseErrorHandler extends DefaultResponseErrorHandler {

        /**
         * Обработчик HTTP-статусов ответа, который не выбрасывает исключения
         * <b>HttpClientErrorException</b> и <b>HttpServerErrorException</b>.
         */
        public static final ResponseErrorHandler INSTANCE = new NoOpResponseErrorHandler();

        @Override
        public void handleError(@NonNull ClientHttpResponse response) {
        }
    }
}
